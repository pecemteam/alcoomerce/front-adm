import React from "react";
import {Admin, Resource} from 'react-admin';
import {theme, initialState} from "./styles";
import {authProvider, dataProvider, i18nProvider} from '../../utils';
import NotFound from '../../components/NotFound';

import CustomLayout from '../../components/Layout';
import {UserList} from "../../components/Users/userList";
import {UserEdit} from "../../components/Users/userEdit";
import {UserCreate} from "../../components/Users/userCreate";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import PeopleIcon from '@material-ui/icons/People';
import {ClientList} from "../../components/Clients/clientsList";
import BookIcon from '@material-ui/icons/Book';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import {CategoryCreate} from "../../components/Categories/categoryCreate";
import {CategoryEdit} from "../../components/Categories/categoryEdit";
import {CategoryList} from "../../components/Categories/categoryList";
import {PurchasesList} from "../../components/Purchases/purchasesList";
import {ProductList} from "../../components/Products/productList";
import DynamicFeedIcon from '@material-ui/icons/DynamicFeed';
import {ProductCreate} from "../../components/Products/productCreate";
import {ProductEdit} from "../../components/Products/productEdit";

export default function AdminPage() {
    return (
        <Admin title="Alcommerce" authProvider={authProvider} dataProvider={dataProvider}
               i18nProvider={i18nProvider} catchAll={NotFound} layout={CustomLayout}
               initialState={initialState}
        >
            <Resource options={{ label: 'Clientes' }} name="clients" list={ClientList} icon={PeopleIcon}/>
            <Resource options={{ label: 'Categorias' }} name="categories" list={CategoryList} edit={CategoryEdit} create={CategoryCreate} icon={BookIcon} />
            <Resource options={{ label: 'Compras' }} name="purchases" list={PurchasesList} icon={ShoppingBasketIcon}/>
            <Resource options={{ label: 'Produtos' }} name="products" list={ProductList} edit={ProductEdit} create={ProductCreate} icon={DynamicFeedIcon} />
            <Resource options={{ label: 'Usuários' }} name="users" list={UserList} edit={UserEdit} create={UserCreate} icon={AccountCircleIcon}/>
        </Admin>
    )
}
