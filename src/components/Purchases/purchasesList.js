import * as React from "react";
import {List, Datagrid, TextField, NumberField, Filter, SearchInput, Pagination, SimpleList} from 'react-admin';
import Toolbar from "@material-ui/core/Toolbar";
import { useMediaQuery } from '@material-ui/core';

const PurchasesFilter = (props) => (
    <Filter {...props}>
        <SearchInput placeholder="Localizar" source="q" alwaysOn/>
    </Filter>
);

const PurchasePrice = ({record}) => {
    return <span>R$ {record.product[0].price}</span>;
};

const PurchasesActions = ({
                         basePath,
                         displayedFilters,
                         filters,
                         filterValues,
                         resource,
                         showFilter,
                     }) => (
    <Toolbar>
        {filters && React.cloneElement(filters, {
            resource,
            showFilter,
            displayedFilters,
            filterValues,
            context: 'button',
        })}

    </Toolbar>
);

const PurchasesPagination = props => <Pagination label="Itens por Página"
                                            rowsPerPageOptions={[5, 10, 15, 20, 30, 50]}  {...props} />;

export const PurchasesList = props => {
    const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));
    return (
        <List title="Lista de Compras" bulkActionButtons={false} exporter filters={<PurchasesFilter/>} actions={<PurchasesActions/>}
              pagination={<PurchasesPagination/>} {...props}>
            {isSmall ? (
                <SimpleList
                    primaryText={record => record.name}
                    secondaryText={record => record.email}
                    tertiaryText={record => (record.admin) ? "ADMIN": ""}
                />
            ) : (
                <Datagrid rowClick="edit">
                    <TextField label="Comprador" source='client.name'/>
                    <TextField label="CPF do Comprador" source='client.cpf'/>
                    <TextField label="Produto" source='product[0].name'/>
                    <TextField label="Categoria do produto" source='product[0].category'/>
                    <PurchasePrice label="Total da Compra" />
                </Datagrid>
            )}

        </List>
    );
}
