import * as React from "react";
import {List, Datagrid, TextField, EmailField, Filter, SearchInput, CreateButton, Pagination, SimpleList} from 'react-admin';
import Toolbar from "@material-ui/core/Toolbar";
import { useMediaQuery } from '@material-ui/core';

const CategoryFilter = (props) => (
  <Filter {...props}>
    <SearchInput placeholder="Localizar" source="q" alwaysOn/>
  </Filter>
);

const CategoryActions = ({
                       basePath,
                       displayedFilters,
                       filters,
                       filterValues,
                       resource,
                       showFilter,
                     }) => (
  <Toolbar>
    {filters && React.cloneElement(filters, {
      resource,
      showFilter,
      displayedFilters,
      filterValues,
      context: 'button',
    })}
    <CreateButton basePath={basePath}/>

  </Toolbar>
);

const CategoryPagination = props => <Pagination label="Itens por Página"
                                            rowsPerPageOptions={[5, 10, 15, 20, 30, 50]}  {...props} />;

export const CategoryList = props => {
  const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));
  return (
    <List title="Lista de Categorias" bulkActionButtons={false} filters={<CategoryFilter/>} actions={<CategoryActions/>}
          pagination={<CategoryPagination/>} {...props}>
      {isSmall ? (
        <SimpleList
          primaryText={record => record.categoryName}
        />
      ) : (
        <Datagrid rowClick="edit">
          <TextField label="Nome da categoria" source="categoryName"/>
        </Datagrid>
      )}

    </List>
  );
}
