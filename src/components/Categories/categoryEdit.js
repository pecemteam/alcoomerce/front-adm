import * as React from "react";
import {
  Edit,
  SimpleForm,
  TextInput,
  Toolbar,
  SaveButton,
  DeleteButton,
  required,
} from 'react-admin';
import {makeStyles} from '@material-ui/core/styles';
import BackButton from "../BackButton";
import {useMediaQuery} from "@material-ui/core";

const CategoryName = ({record}) => {
  return <span>Editar Categoria {record.name}</span>;
};

const useStyles = makeStyles({
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});

const CustomToolbar = props => (
  <Toolbar {...props} classes={useStyles()}>
    <SaveButton label="Salvar"/>
    <DeleteButton resource="categories" undoable={true}/>
  </Toolbar>
);

const TopToolBar = props => (
  <Toolbar {...props} classes={useStyles()}>
    <BackButton
      color='secondary'
    >
      Voltar
    </BackButton>
  </Toolbar>
);
export const CategoryEdit = ({ permissions, ...props }) => {
  const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));
  return (
    <Edit title={<CategoryName/>} actions={(isSmall)? <BackButton color='secondary'>Voltar</BackButton>
      : <TopToolBar/>} {...props}>
      <SimpleForm redirect="list" toolbar={<CustomToolbar/>}>
        <TextInput label="Nome da Categoria" source="categoryName" validate={[required()]}/>
      </SimpleForm>
    </Edit>
  );
}
