import * as React from "react";
import {Create, SimpleForm, TextInput, Toolbar, SaveButton, required} from 'react-admin';
import {makeStyles} from '@material-ui/core/styles';
import BackButton from "../BackButton";
import {useMediaQuery} from "@material-ui/core";

const useStyles = makeStyles({
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});

const CustomToolbar = props => (
  <Toolbar {...props} classes={useStyles()}>
    <SaveButton label="Criar"/>
  </Toolbar>
);

const TopToolBar = props => (
  <Toolbar {...props} classes={useStyles()}>
    <BackButton
      color='secondary'
    >
      Voltar
    </BackButton>
  </Toolbar>
);

export const CategoryCreate = props => {
  const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));
  return (
    <Create title="Criar Categoria" actions={(isSmall)? <BackButton color='secondary'>Voltar</BackButton>
      : <TopToolBar/>}{...props}>
      <SimpleForm redirect="list" toolbar={<CustomToolbar/>}>
        <TextInput label="Nome da Categoria" source="categoryName" validate={[required()]}/>
      </SimpleForm>
    </Create>
  );
}
