import * as React from "react";
import {List, Datagrid, TextField, EmailField, Filter, SearchInput, Pagination, SimpleList} from 'react-admin';
import Toolbar from "@material-ui/core/Toolbar";
import { useMediaQuery } from '@material-ui/core';

const ClientFilter = (props) => (
    <Filter {...props}>
        <SearchInput placeholder="Localizar" source="q" alwaysOn/>
    </Filter>
);

const ClientNumberPurchases = ({record}) => {
    return <span>{record.purchases.length}</span>;
};

const ClientActions = ({
                         basePath,
                         displayedFilters,
                         filters,
                         filterValues,
                         resource,
                         showFilter,
                     }) => (
    <Toolbar>
        {filters && React.cloneElement(filters, {
            resource,
            showFilter,
            displayedFilters,
            filterValues,
            context: 'button',
        })}

    </Toolbar>
);

const ClientPagination = props => <Pagination label="Itens por Página"
                                            rowsPerPageOptions={[5, 10, 15, 20, 30, 50]}  {...props} />;

export const ClientList = props => {
    const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));
    return (
        <List title="Lista de Clientes" bulkActionButtons={false} filters={<ClientFilter/>} actions={<ClientActions/>}
              pagination={<ClientPagination/>} {...props}>
            {isSmall ? (
                <SimpleList
                    primaryText={record => record.name}
                    secondaryText={record => record.email}
                    tertiaryText={record => (record.cpf)}
                />
            ) : (
                <Datagrid rowClick="edit">
                    <TextField label="Nome" source="name"/>
                    <EmailField label="Email" source="email"/>
                    <TextField label="CPF" source="cpf"/>
                    <TextField label="Estado" source="estado"/>
                    <TextField label="Cidade" source="cidade"/>
                    <ClientNumberPurchases label="Número de Compras" />
                </Datagrid>
            )}

        </List>
    );
}
